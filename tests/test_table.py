#!/usr/bin/env python3

from unittest import TestCase
from shelltable import Table

class DummyTable:
    _columns = [('id', 4), ('name', -1), ('value', 8)]
    _rows = [
        ('01', 'idk', '112.05'),
        ('88', 'mmmmm', '3'),
        ('B7', 'test', '43')
    ]
    _default_style = '-|(_)[;]<=>'
    _buffer = ('(----_--------_--------)\n'
               '|id  |name    |value   |\n'
               '[----;--------;--------]\n'
               '|01  |idk     |112.05  |\n'
               '[----;--------;--------]\n'
               '|88  |mmmmm   |3       |\n'
               '[----;--------;--------]\n'
               '|B7  |test    |43      |\n'
               '<----=--------=-------->\n')

    @classmethod
    def render(cls, *, style=''):
        table = cls._buffer
        for a, b in zip(cls._default_style, style):
            table = table.replace(a, b)
        return table

    @classmethod
    def make_one(cls, *, style=None):
        table = Table()
        table.set_width(24)
        table.set_style(style or cls._default_style)
        for c in cls._columns:
            table.add_column(c[0], size=c[1])
        for r in cls._rows:
            table.add_row(*r)
        return table

class TestTable(TestCase):
    def test_prerendered_table(self):
        fake_tab = DummyTable.render()
        real_tab = DummyTable.make_one()
        self.assertEqual(str(real_tab), fake_tab)

    def test_width(self):
        table = DummyTable.make_one()
        for l in range(30, 60, 9):
            table.set_width(l)
            buf = str(table)
            lines_length = list(map(len, buf.split('\n')[:-1]))
            self.assertEqual(lines_length, [l]*len(lines_length))

    def test_style_table(self):
        styles = (v for k, v in Table.__dict__.items() if k.startswith('STYLE_'))
        for style in styles:
            generated = str(DummyTable.make_one(style=style))
            prerendered = DummyTable.render(style=style)
            self.assertEqual(generated, prerendered)

